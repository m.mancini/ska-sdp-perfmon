Perfmon Reference
==============================

Configuration
---------------

.. automodule:: perfmon.cfg.__init__
    :members:

Common
---------

.. automodule:: perfmon.common.df.__init__
   :members:

.. automodule:: perfmon.common.export.__init__
    :members:

.. automodule:: perfmon.common.logging.__init__
    :members:

.. automodule:: perfmon.common.perf.__init__
   :members:

.. automodule:: perfmon.common.pid.__init__
    :members:

.. automodule:: perfmon.common.plots.__init__
    :members:

.. automodule:: perfmon.common.processor.__init__
   :members:

.. automodule:: perfmon.common.report.__init__
    :members:

.. automodule:: perfmon.common.utils.devices
    :members:

.. automodule:: perfmon.common.utils.execute_cmd
   :members:

.. automodule:: perfmon.common.utils.json_wrappers
    :members:

.. automodule:: perfmon.common.utils.locks
    :members:

.. automodule:: perfmon.common.utils.parsing
    :members:

.. automodule:: perfmon.common.utils.pdf
   :members:

.. automodule:: perfmon.common.utils.process
    :members:

.. automodule:: perfmon.common.utils.utilities
    :members:


Core
-------

.. automodule:: perfmon.core.metrics.__init__
   :members:

.. automodule:: perfmon.core.metrics.common
   :members:

.. automodule:: perfmon.core.metrics.cpu
   :members:

.. automodule:: perfmon.core.metrics.gpu
  :members:

.. automodule:: perfmon.core.metrics.perfcounters
   :members:

.. automodule:: perfmon.core.metrics.cpumetrics.energy
   :members:

.. automodule:: perfmon.core.metrics.cpumetrics.memory
   :members:

.. automodule:: perfmon.core.metrics.cpumetrics.misc
   :members:

.. automodule:: perfmon.core.metrics.cpumetrics.network
   :members:

.. automodule:: perfmon.core.metrics.cpumetrics.usage
   :members:

.. automodule:: perfmon.core.metrics.gpumetrics.nvidia.__init__
  :members:

.. automodule:: perfmon.core.metrics.gpumetrics.nvidia.clock
  :members:

.. automodule:: perfmon.core.metrics.gpumetrics.nvidia.errors
  :members:

.. automodule:: perfmon.core.metrics.gpumetrics.nvidia.memory
  :members:

.. automodule:: perfmon.core.metrics.gpumetrics.nvidia.misc
  :members:

.. automodule:: perfmon.core.metrics.gpumetrics.nvidia.power
  :members:

.. automodule:: perfmon.core.metrics.gpumetrics.nvidia.utilization
  :members:



Exceptions
------------

.. automodule:: perfmon.exceptions.__init__
   :members:


Perfevents
------------

.. automodule:: perfmon.perfevents.__init__
   :members:


Schemas
-----------

.. automodule:: perfmon.schemas.__init__
   :members:

.. automodule:: perfmon.schemas.df
  :members:

.. automodule:: perfmon.schemas.metrics
   :members:

.. automodule:: perfmon.schemas.plots
  :members:
