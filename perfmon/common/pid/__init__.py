"""This module contains class for detecting process PIDs for various schedulers"""

import os
import logging

from perfmon.common.pid.pid import find_job_pid
from perfmon.exceptions import JobPIDNotFoundError

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


class GetJobPid(object):
    """Class to get the main job PID for different workload managers. Currently SLURM, PBS and OAR
    schedulers are supported
    """

    def __init__(self, config):

        self.config = config.copy()

        # Initiliase scheduler config dict
        self.scheduler = {}

        # Get username
        self.scheduler['user_name'] = os.environ['USER']
        # Get PID of the current script
        self.scheduler['current_pid'] = os.getpid()
        # Get scheduler name
        self.scheduler['name'] = config['scheduler']
        # Get job ID
        self.scheduler['job_id'] = config['job_id']
        # Get current node's hostname
        self.scheduler['node_name'] = config['node_name']
        # Get SLURM submit node
        self.scheduler['master_node'] = config['master_node']
        # Get launcher type
        self.scheduler['launcher'] = config['launcher']
        # Get script name
        self.scheduler['script_names'] = [
            config['script_name'],
            'python',
            'python3',
            'coverage',
        ]

        # define timeout (sec)
        self.scheduler['timeout'] = 5
        # define maximum try outs (sec)
        self.scheduler['max_retries'] = 5

    def go(self):
        """This is driver method to find job PID"""

        try:
            # Try to find PID from scheduler specific implementation
            pid = find_job_pid(self.scheduler['name'].lower(), self.scheduler)
        except JobPIDNotFoundError:
            pid = find_job_pid('naive', self.scheduler)

        return pid
