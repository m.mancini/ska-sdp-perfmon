""""This package contains functions to export metric data in CSV format"""

import os
import logging

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def csv_exporter(config, df_dict):
    """This method exports the dataframe data into CSV file"""

    # Open writer
    for metric, df in df_dict.items():
        # Check if df is not None
        if df.empty:
            _log.debug('No %s data found to export to excel. Skipping' % metric)
            continue
        file_name = os.path.join(config['save_dir'], ".".join([metric, 'csv']))
        df.to_csv(
            path_or_buf=file_name,
            na_rep='NaN',
            float_format='%.3f',
        )
