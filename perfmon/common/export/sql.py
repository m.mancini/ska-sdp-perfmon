""""This package contains classes to export metric data"""

import logging
import sqlite3

from perfmon.common.export.export import get_tag

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def register_sql(config, df_dict):
    """This method exports the dataframe data into database"""

    # Initialise connection to DB
    conn = sqlite3.connect(config['db_file'])
    for metric, df in df_dict.items():
        if df.empty:
            _log.debug('No %s data found to export to excel. Skipping' % metric)
            continue
        df.to_sql(
            get_tag(metric, config['job_id']), conn, if_exists='append', index=False
        )
