""""This package contains classes to export metric data"""

import logging

from importlib import import_module

from perfmon.common.utils.dispatch import Dispatch

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301

_avail_exporters = [
    'csv',
    'hdf5',
    'parquet',
    'feather',
    'pickle',
    'orc',
]

_exporter_mods = {}
for exporter in _avail_exporters:
    _exporter_mods[exporter] = f'perfmon.common.export.{exporter}'


def get_tag(metric, job_id):
    """Tag metric with job ID"""
    return '_'.join([metric, str(job_id)])


@Dispatch
def data_exporter(name, config, df_dict):
    """Export metric data to a given format"""
    _log.warning('Data exporter format not recognised or not implemented. Skipping...')


@data_exporter.register('csv')
def register_csv(name, config, df_dict):
    """Register CSV exporter function"""
    mod = import_module(_exporter_mods[name])
    getattr(mod, f'{name}_exporter')(config, df_dict)


@data_exporter.register('hdf5')
def register_hdf5(name, config, df_dict):
    """Register HDF5 exporter function"""
    mod = import_module(_exporter_mods[name])
    getattr(mod, f'{name}_exporter')(config, df_dict)


@data_exporter.register('parquet')
def register_parquet(name, config, df_dict):
    """Register parquet exporter function"""
    mod = import_module(_exporter_mods[name])
    getattr(mod, f'{name}_exporter')(config, df_dict)


@data_exporter.register('feather')
def register_feather(name, config, df_dict):
    """Register feather exporter function"""
    mod = import_module(_exporter_mods[name])
    getattr(mod, f'{name}_exporter')(config, df_dict)


@data_exporter.register('pickle')
def register_pickle(name, config, df_dict):
    """Register pickle exporter function"""
    mod = import_module(_exporter_mods[name])
    getattr(mod, f'{name}_exporter')(config, df_dict)


@data_exporter.register('orc')
def register_orc(name, config, df_dict):
    """Register ORC exporter function"""
    mod = import_module(_exporter_mods[name])
    getattr(mod, f'{name}_exporter')(config, df_dict)
