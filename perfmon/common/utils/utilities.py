"""Utility functions"""

import os
import pathlib
import logging
import yaml

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def get_project_root():
    """Get root directory of the project

    Returns:
        str: Full path of the root directory
    """
    return pathlib.Path(__file__).parent.parent.parent


def merge_dicts(exst_dict, new_dict):
    """Merge two dicts. old_content is updated with data from new_content

    Args:
        exst_dict(dict): Existing data in the dict
        new_dict(dict): New data to be added to the dict
    Returns:
        dict: updated exst_dict with contents from new_dict
    """

    for key, value in exst_dict.items():
        if isinstance(value, list):
            value.extend(new_dict[key])
        elif isinstance(value, dict):
            merge_dicts(exst_dict[key], new_dict[key])

    return exst_dict


def get_value(input_dict, target):
    """Find the value for a given target in dict

    Args:
        input_dict (dict): Dict to search for key
        target (Any): Key to search
    Returns:
        list: List of values found in d
    """

    val = filter(
        None,
        [
            [b] if a == target else get_value(b, target) if isinstance(b, dict) else None
            for a, b in input_dict.items()
        ],
    )

    return [i for b in val for i in b]


def replace_negative(input_list):
    """This function replaces the negative values in numpy array with mean of neighbours. If the
    values happen to be at the extremum, it replaces with preceding or succeding elements

    Args:
        input_list (list): A list with positive and/or negative elements
    Returns:
        list: A list with just positive elements
    """

    len_list = len(input_list)

    out_list = []
    for ielem, elem in enumerate(input_list):
        if elem < 0 and (0 < ielem < len_list - 1):
            out_list.append(0.5 * (input_list[ielem - 1] + input_list[ielem + 1]))
        elif elem < 0 and ielem == 0:
            out_list.append(input_list[ielem + 1])
        elif elem < 0 and ielem == len_list - 1:
            out_list.append(input_list[ielem - 1])
        else:
            out_list.append(elem)

    return out_list


def dump_yaml(config):
    """Dump config files (for debugging)"""

    # Create a directory to save config files
    os.makedirs(os.path.join(config['save_dir'], 'configs'), exist_ok=True)
    # Save running config
    with open(
        os.path.join(
            config['save_dir'], 'configs', '_'.join([config['host_name'], 'run_config.yml'])
        ),
        'w',
    ) as conf_file:
        yaml.dump(config, conf_file)
