"""This module contains functions related to processor specific info"""

import logging
import archspec.cpu

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def get_cpu_spec():
    """
    This function extracts the vendor and cpu architectures using ``archspec`` module

    Returns:
        str: Name of the vendor
        str: Micro architecture
    """

    # First get processor vendor and model details
    vendor_name = archspec.cpu.detect.host().vendor

    # Get micro architecture name
    micro_architecture = archspec.cpu.detect.host().name

    return vendor_name, micro_architecture
