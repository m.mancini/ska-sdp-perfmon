"""This file contains functions to parse CLI arguments"""
import os
import argparse
import logging

from perfmon.common.utils.parsing import RawFormatter

# pylint: disable=E0401,W0201,C0301


def parse_args(cmd_args=None):
    """Parse arguments from CLI"""

    # create parser
    parser = argparse.ArgumentParser(formatter_class=RawFormatter)
    parser.add_argument(
        '-d',
        '--save_dir',
        default=os.getcwd(),
        nargs='?',
        help='''R|
        Base directory where metrics will be saved. This directory should 
        be available from all compute nodes. Default is $PWD
    ''',
    )

    parser.add_argument(
        '-p',
        '--prefix',
        default=None,
        nargs='?',
        help='''R|
            Name of the directory to be created to save metric data. If provided, 
            metrics will be located at $SAVE_DIR/$PREFIX.
        ''',
    )

    parser.add_argument(
        '-l',
        '--launcher',
        nargs='?',
        type=str,
        default=None,
        help='''R|
        Launcher used to launch mpi tasks
    ''',
    )

    parser.add_argument(
        '-i',
        '--sampling_freq',
        type=int,
        default=30,
        help='''R|
        Sampling interval to collect metrics. Default value is 30 seconds
    ''',
    )

    parser.add_argument(
        '-c',
        '--check_point',
        nargs='?',
        type=int,
        default=900,
        help='''R|
        Checking point time interval. Default value is 900 seconds
    ''',
    )

    parser.add_argument(
        '-r',
        '--gen_report',
        action='store_true',
        default=False,
        help='''R|
        Generate plots and job report
    ''',
    )

    parser.add_argument(
        '-e',
        '--export',
        nargs='+',
        choices=['csv', 'hdf5', 'parquet', 'pickle', 'feather', 'orc'],
        default=None,
        help='''R|
        Export results to different file formats
    ''',
    )

    parser.add_argument(
        '--system',
        nargs='?',
        type=str,
        default=None,
        help='''R|
        Name of the system (only when used with SDP Benchmark tests)
    ''',
    )

    parser.add_argument(
        '--partition',
        nargs='?',
        type=str,
        default=None,
        help='''R|
        Name of the partition (only when used with SDP Benchmark tests)
    ''',
    )

    parser.add_argument(
        '--env',
        nargs='?',
        type=str,
        default=None,
        help='''R|
        Name of the environment (only when used with SDP Benchmark tests)
    ''',
    )

    parser.add_argument(
        '--name',
        nargs='?',
        type=str,
        default=None,
        help='''R|
        Name of the test (only when used with SDP Benchmark tests)
    ''',
    )

    parser.add_argument(
        '-v',
        '--verbose',
        action='store_true',
        default=False,
        help='''R|
        Enable verbose mode. Display debug messages
    ''',
    )

    # parse given arguments
    args = parser.parse_args(cmd_args)

    # convert some fields
    if args.verbose:
        args.log_level = logging.DEBUG

    return args
