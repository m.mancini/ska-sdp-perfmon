"""This file contains classes to setup scheduler related steps"""

import os
import logging
import psutil

from perfmon.common.utils.execute_cmd import execute_cmd
from perfmon.exceptions import BatchSchedulerNotFound

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


class SetUpScheduler(object):
    """Set up all the required steps related to scheduler"""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, config):
        """Initialize setup"""

        self.config = config

    def get_job_id(self):
        """Find the workload manager and job ID. Exit if none found"""

        # Check if job ID can be found from SLURM/PBS/OAR
        # If no workload manager found, exit
        if 'SLURM_JOB_ID' in os.environ.keys():
            self.config['job_id'] = int(os.environ['SLURM_JOB_ID'])
            self.config['scheduler'] = 'SLURM'
            _log.info('SLURM job with ID %d detected', self.config['job_id'])
        elif 'PBS_JOBID' in os.environ.keys():
            self.config['job_id'] = os.environ['PBS_JOBID']
            self.config['scheduler'] = 'PBS'
            _log.info('PBS job with ID %s detected', self.config['job_id'])
        elif 'OAR_JOB_ID' in os.environ.keys():
            self.config['job_id'] = int(os.environ['OAR_JOB_ID'])
            self.config['scheduler'] = 'OAR'
            _log.info('OAR job with ID %d detected', self.config['job_id'])
        elif 'LOCAL_JOB_ID' in os.environ.keys():
            self.config['job_id'] = int(os.environ['LOCAL_JOB_ID'])
            self.config['scheduler'] = 'LOCAL'
            _log.info('LOCAL job with ID %d detected', self.config['job_id'])
        else:
            _log.warning(
                'Workload manager not recognised. Job PIDs will be '
                'found by iterating over all user PIDs'
            )

    def create_node_list(self):
        """Create a list of node names"""

        # Get node names
        self.config['node_name'] = self.config['host_name']

        if self.config['scheduler'] == 'SLURM':
            # Command to execute
            cmd_str = (
                f"sinfo -N --nodes={os.environ['SLURM_JOB_NODELIST']} --format=\'%N\' | awk \'{{"
                f"if (NR>1)print}}\'"
            )
        elif self.config['scheduler'] == 'PBS':
            # Command to execute
            # PBS nodefile is only available on head node. So other nodes in the reservation cannot
            # access this file. The hack is we copy the nodefile to CWD and define a new env
            # variable and export it using mpirun
            # On head node:
            # cp #PBS_NODEFILE nodefile
            # mpirun -x PBS_NODEFILE_LOCAL=$PWD/nodefile <snippet>
            cmd_str = 'uniq $PBS_NODEFILE_LOCAL'
        elif self.config['scheduler'] == 'OAR':
            # Command to execute
            cmd_str = 'uniq $OAR_NODEFILE'
        elif self.config['scheduler'] == 'LOCAL':
            # Command to execute
            cmd_str = 'hostname'
        else:
            _log.error('Workload manager not identified.')
            raise BatchSchedulerNotFound('Batch scheduler not found')

        # Execute command and parse node name list
        self.config['node_name_list'] = execute_cmd(cmd_str).splitlines()

        _log.debug(
            'Found current node name is %s and node list is %s'
            % (self.config['node_name'], self.config['node_name_list'])
        )

        # Sometimes hostname and nodelist have different names, mostly due to adding
        # local network name to the hostname. We try to make them same here
        if self.config['node_name'] not in self.config['node_name_list']:
            # This case is when node_name is my-node-0 and node name in node_name_list is my-node-0.local
            check_0 = [s for s in self.config['node_name_list'] if self.config['node_name'] in s]
            if check_0:
                self.config['node_name'] = check_0[0]
            # This case is when node_name is my-node-0.local and node name in node_name_list is my-node-0
            check_1 = [s for s in self.config['node_name_list'] if s in self.config['node_name']]
            if check_1:
                self.config['node_name'] = check_1[0]

        # Get master node, ie, first node in the reservation if node names are not equal
        self.config['master_node'] = self.config['node_name_list'][0]
        _log.debug(
            'Master is %s and current is %s'
            % (self.config['master_node'], self.config['node_name'])
        )

    def find_launcher_type(self):
        """Find which launcher is used to launch MPI jobs"""

        if self.config['launcher'] is None:
            # PID of current process
            current_pid = os.getpid()

            # Get parent of the current process
            proc = psutil.Process(current_pid)
            ppid = proc.ppid()

            # Get name of the parent process
            pp_name = psutil.Process(ppid).name()

            _log.debug(
                'Current process object is %s and its parent process is %s'
                % (proc, psutil.Process(ppid))
            )

            if pp_name in ['mpirun', 'orted']:
                self.config['launcher'] = 'mpirun'
            elif pp_name in ['mpiexec', 'hydra_pmi_proxy']:
                self.config['launcher'] = 'mpiexec'
            elif pp_name in ['slurmstepd', 'psid']:  # On JUWELS, psid launches tasks
                self.config['launcher'] = 'srun'
        else:
            _log.debug('Launcher type %s specified on CLI' % self.config['launcher'])

    def go(self):
        """Preprocessing steps of scheduler definition"""

        # Find Job id
        self.get_job_id()

        # Create a list of node names
        self.create_node_list()

        # Find launcher type mpirun or srun
        self.find_launcher_type()

        return self.config
