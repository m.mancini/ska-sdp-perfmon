"""This file contains classes to setup Inter process Comunicator (IPC)"""

import os
import time
import logging

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


class SetUpIPC(object):
    """Set up all the required steps related to IPC creation"""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, config):
        """Initialize setup"""

        self.config = config

    def create_ipc_file(self):
        """Create Inter process communicator (IPC) file. It will be used to signal the toolkit
        that main job has ended"""

        # Name of the IPC file
        self.config['ipc_file'] = '-'.join(['.ipc', str(self.config['job_id'])])

        # Create an ipc file only by the master node
        if self.config['master_node'] == self.config['node_name']:
            # We create a hidden file with text INPROGRESS to signal that job is running
            # Once job is finished there will be line `echo 'FINISHED' > .ipc`
            with open(self.config['ipc_file'], 'w') as ipc_file:
                ipc_file.write('INPROGRESS')
        else:
            # File is created on the master node and slaves nodes wait till the file is
            # created. This is barrier to synchronize all processes
            timeout = 600
            start_time = time.time()
            elapsed_time = 0
            while not os.path.isfile(self.config['ipc_file']) and not elapsed_time > timeout:
                time.sleep(1)
                elapsed_time = time.time() - start_time

    def go(self):
        """Preprocessing steps of data collection"""

        # Create a inter process communicator (IPC) file
        self.create_ipc_file()

        return self.config
