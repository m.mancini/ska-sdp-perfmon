"""This file contains convienence wrappers to orchestrate the monitoring process"""

import sys
import logging

from perfmon.core.setup.setup_lock import SetUpLockFiles
from perfmon.core.execution import SetUpMonitor
from perfmon.core.execution import PostMonitoring
from perfmon.core.metadata.wrapper import get_hw_metadata
from perfmon.core.metrics import MonitorPerformanceMetrics
from perfmon.common.plots import GenPlots
from perfmon.common.report import GenReport
from perfmon.common.df import CreateDataFrame
from perfmon.common.export import ExportData
from perfmon.common.utils.utilities import dump_yaml

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def setup_monitoring(global_config):
    """This function is a wrapper around SetUpMonitor to kick start
    monitoring process
    """

    # Pre processing steps
    setup_monitor = SetUpMonitor(config=global_config)
    try:
        _log.info('Setting up monitoring....')
        global_config = setup_monitor.go()
        _log.info('Monitoring setup finished')
    except Exception:
        _log.warning('Monitoring setup failed', exc_info=sys.exc_info())

    # Pre processing steps
    setup_lock = SetUpLockFiles(config=global_config)
    try:
        _log.info('Setting up lock files....')
        global_config = setup_lock.lock()
        _log.info('Locks setup finished')
    except Exception:
        _log.warning('Locks setup failed', exc_info=sys.exc_info())

    return global_config, setup_lock


def perfmon_exec_engine(global_config):
    """Main execution engine for perfmon"""

    # Get hardware metadata
    try:
        _log.info('Getting hardware metadata....')
        get_hw_metadata(global_config)
        _log.info('Hardware metadata extracted')
    except Exception:
        _log.warning('Getting hardware metadata failed', exc_info=sys.exc_info())

    # Monitor perf metrics
    collect_metrics = MonitorPerformanceMetrics(config=global_config)
    try:
        _log.info('Starting monitoring....')
        collect_metrics.start_collection()
    except Exception:
        _log.warning('Monitoring failed', exc_info=sys.exc_info())


def teardown_monitoring(global_config, set_up_monitor):
    """This function is a wrapper around SetUpMonitor to stop the
    monitoring process
    """

    # Shutdown the process monitors
    try:
        _log.info('Stopping monitoring....')
        set_up_monitor.shutdown()
        _log.info('Monitoring finished')
    except Exception:
        _log.warning('Monitoring tear down failed.', exc_info=sys.exc_info())

    # Post monitoring steps
    post_monitor = PostMonitoring(config=global_config)
    try:
        _log.info('Performing post monitoring steps....')
        post_monitor.go()
        _log.info('Post monitoring steps finished')
    except Exception:
        _log.warning('Post monitoring steps failed', exc_info=sys.exc_info())


def postprocess_engine(global_config):
    """Main engine for postprocessing

    Args:
        global_config (dict): Configuration dict
    """

    # Get all avail metrics
    metrics = global_config['metrics'].copy()
    metrics.remove('meta_data')

    # Create dataframe from metric data
    df = {}
    for metric in metrics:
        metric_df = CreateDataFrame(metric=metric, config=global_config)
        df[metric] = metric_df.go()

    # Export results to excel, SQL and HDF5 datastore
    if global_config['export']:
        export_data = ExportData(config=global_config, df_dict=df)
        export_data.go()

    if global_config['gen_report']:
        # Run MakePlots class
        make_plots = GenPlots(config=global_config, df_dict=df)
        make_plots.go()

        # Generate report
        gen_report = GenReport(config=global_config)
        gen_report.go()


def postprocess_metric_data(global_config):
    """Classes to be called in postprocessing

    Args:
        global_config (dict): Configuration dict
    """

    if global_config['verbose']:
        dump_yaml(global_config)

    # We want to run these steps only on one node. We use master node
    if global_config['master_node'] == global_config['node_name']:
        try:
            _log.info('Starting post processing of data....')
            postprocess_engine(global_config)
            _log.info('Post processing finished')
        except Exception:
            _log.warning('Metric data post processing failed', exc_info=sys.exc_info())


def perfmon_entrypoint(global_config):
    """Entry point of perfmon lib

    Args:
        global_config (dict): Configuration dict
    """

    # Set up monitoring process
    _log.info("Setting up monitoring....")
    global_config, set_up_monitor = setup_monitoring(global_config)

    # Main execution engine for perfmon
    _log.info("Entering the execution engine of perfmon....")
    perfmon_exec_engine(global_config)

    # Tear down monitoring process
    _log.info("Tear down monitoring....")
    teardown_monitoring(global_config, set_up_monitor)

    # Finish monitoring by post processing the data
    _log.info("Performing post process steps....")
    postprocess_metric_data(global_config)
