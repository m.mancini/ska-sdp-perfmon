"""Functions to monitor clock frequency info related metrics for NVIDIA GPUs"""

import logging

from py3nvml.py3nvml import *

from perfmon.core.metrics.gpumetrics.nvidia import device_query

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def clock_info(data):
    """This method gets NVIDIA GPU clock info for memory, graphics and SM"""

    # Get graphics clock info
    graphics_report = device_query('nvmlDeviceGetClockInfo', 0)
    for i, grap in enumerate(graphics_report):
        data[i]['clock_info']['graphics'].append(grap)

    # Get SM clock info
    sm_report = device_query('nvmlDeviceGetClockInfo', 1)
    for i, sm in enumerate(sm_report):
        data[i]['clock_info']['sm'].append(sm)

    # Get graphics clock info
    mem_report = device_query('nvmlDeviceGetClockInfo', 2)
    for i, mem in enumerate(mem_report):
        data[i]['clock_info']['memory'].append(mem)

    return data
