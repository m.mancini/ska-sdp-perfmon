"""Functions to monitor other metrics"""

import logging

from perfmon.core.metrics.common import get_cumulative_metric_value

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def misc_metrics(procs, data):
    """This method gets IO, file descriptors and thread count"""

    # Get cumulative IO counters for the process and its childs
    data = get_cumulative_metric_value('io_counters', procs, data)

    # Get cumulative number of threads for the process and its childs
    data = get_cumulative_metric_value('num_threads', procs, data)

    # Get cumulative number of file descriptors for the process and its childs
    data = get_cumulative_metric_value('num_fds', procs, data)

    return data
