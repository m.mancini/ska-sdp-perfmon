"""Version information."""

# The following line *must* be the last in the module, exactly as formatted:
__version__ = "2.2.0"
__version_info__ = tuple([int(num) for num in __version__.split('.')])
