"""This file contains version and author info"""


from ._version import (
    __version__,
    __version_info__,
)


__author__ = 'Mahendra Paipuri'
__email__  = 'mahendra.paipuri@inria.fr'
