"""This file contains the custom exceptions defined for monitoring tools."""


class CommandExecutionFailed(Exception):
    """Command execution exception"""
    pass


class BatchSchedulerNotFound(Exception):
    """Batch scheduler not implemented or not recognised"""
    pass


class JobPIDNotFoundError(Exception):
    """Step job PID not found"""
    pass


class ProcessorVendorNotFoundError(Exception):
    """Processor vendor not implemented"""
    pass


class KeyNotFoundError(Exception):
    """Key not found in the dict"""
    pass


class PerfEventListNotFoundError(Exception):
    """Perf event list not implemented"""
    pass


class MetricGroupNotImplementedError(Exception):
    """Requested metric group not implemented"""
    pass


class ArchitectureNotFoundError(Exception):
    """Processor architecture not found"""
    pass
