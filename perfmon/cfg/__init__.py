"""This file contains config related functions and classes"""

import os
import sys
import pathlib
import socket
import subprocess


__root__ = str(pathlib.Path(__file__).parent.parent.parent)
__cfg__ = os.path.join(__root__, 'cfg')
__cwd__ = os.getcwd()
__main_py__ = sys.argv[0]

# pylint: disable=E0401,W0201,C0301


class GlobalConfiguration(object):
    """
    Global configuration with defaults
    """

    def __init__(self, args):

        # Convert arg parser namespace to dict
        self.global_config = vars(args)

    def populate_config(self):
        """
        This method adds necessary common info to config dict
        """

        # name of the host
        self.global_config['host_name'] = socket.gethostname()
        # default metrics to collect
        self.global_config['metrics'] = [
            'meta_data', 'cpu_metrics', 'perf_metrics']
        # Get name of the script
        self.global_config['script_name'] = os.path.basename(__main_py__)

    def check_gpus(self):
        """
        This method checks for presence of NVIDIA GPUs
        """

        # check if nvidia-smi command works
        try:
            subprocess.check_output('nvidia-smi')
            import py3nvml

            py3nvml.py3nvml.nvmlInit()
            self.global_config['num_nvidia_gpus'] = int(
                py3nvml.py3nvml.nvmlDeviceGetCount()
            )
            self.global_config['metrics'] += ['nv_gpu_metrics']
            py3nvml.py3nvml.nvmlShutdown()
        except Exception:  # this command not being found can raise quite a few different
            # errors depending on the configuration
            self.global_config['num_nvidia_gpus'] = 0

        try:
            # check if rocm-smi program works
            subprocess.check_output('rocm-smi')

            # check if pypowersensor is installed
            import pypowersensor as pps

            # Workaround to find number of AMD gpus installed on the system
            a = True
            i = 0
            while a:
                val = subprocess.run(
                    [sys.executable, "-c", f"import pypowersensor as pps; pps.ROCMPowerSensor.create({i})"], capture_output=True)
                if val.returncode != 0:
                    a = False
                else:
                    i += 1
            self.global_config['num_amd_gpus'] = i
            if i:
                self.global_config['metrics'] += ['amd_gpu_metrics']

        except Exception as e:
            # Can raise errors depending on configurations
            # Usually means, there is no AMD gpu available.
            # print("Got exception: ", e)
            self.global_config['num_amd_gpus'] = 0

    def make_dirs(self):
        """
        This method creates the directory that put all artefacts of toolkit
        """

        # Get absolute path of the save directory
        self.global_config['save_dir'] = os.path.abspath(
            os.path.expanduser(os.path.expandvars(
                self.global_config['save_dir']))
        )

        # Create save_dir if not already there
        try:
            os.makedirs(self.global_config['save_dir'], exist_ok=False)
        except FileExistsError:
            pass

    def create_config(self):
        """
        Entry point of the class
        """

        # Add aux parameters to global_config
        self.populate_config()

        # Check if GPUs are there on nodes
        self.check_gpus()

        # Make head directory to place all metric data
        self.make_dirs()

        return self.global_config
